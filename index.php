<?php
require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");

echo "Nama Binatang : " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $sheep->legs . "<br>"; // 4
echo "Apakah berdarah dingin : " . $sheep->cold_blooded . "<br>"; // "no"

echo "<br>";

$kodok = new frog("buduk");
echo "Nama Binatang : " . $kodok->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $kodok->legs . "<br>"; // 4
echo "Apakah berdarah dingin : " . $kodok->cold_blooded . "<br>"; // "no
$kodok->jump(); // "hop hop"

echo "<br>";
echo "<br>";

$sungokong = new ape("kera sakti");
echo "Nama Binatang : " . $sungokong->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $sungokong->legs . "<br>"; // 4
echo "Apakah berdarah dingin : " . $sungokong->cold_blooded . "<br>"; // "no
$sungokong->yell(); // "Auooo"
